.. meta::
    :description:
        Guide to Running Krita.

.. metadata-placeholder

    :authors: - Halla Rempt <boud@valdyas.org>
    :license: GNU free documentation license 1.3 or later.
    
.. _running_krita:

==========================
Running Krita from Source
==========================
