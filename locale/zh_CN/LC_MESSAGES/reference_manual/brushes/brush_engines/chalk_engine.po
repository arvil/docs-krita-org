msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___chalk_engine."
"pot\n"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:1
msgid "The Chalk Brush Engine manual page."
msgstr "介绍 Krita 的粉笔笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:17
msgid "Chalk Brush Engine"
msgstr "粉笔笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:21
msgid ""
"This brush engine has been removed in 4.0. There are other brush engines "
"such as pixel that can do everything this can...plus more."
msgstr ""
"此笔刷引擎已经在 4.0 版中被移除。我们的像素笔刷引擎不但可以实现同样效果，而且"
"功能更加丰富。"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:23
msgid ""
"Apparently, the Bristle brush engine is derived from this brush engine. Now, "
"all of :program:`Krita's` brushes have a great variety of uses, so you must "
"have tried out the Chalk brush and wondered what it is for. Is it nothing "
"but a pixel brush with opacity and saturation fade options? As per the "
"developers this brush uses a different algorithm than the Pixel Brush, and "
"they left it in here as a simple demonstration of the capabilities of :"
"program:`Krita's` brush engines."
msgstr ""
"虽然这个引擎已被移除，但我们保留了此页面来展示 Krita 笔刷引擎的潜力。你或许不"
"会想到，Krita 的鬃毛笔刷引擎竟然是从这个引擎的基础上发展出来的。它和像素引擎"
"使用了不同的算法，但看上去只是一个带有不透明度和饱和度淡化功能的像素笔刷。"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:26
msgid ""
"So there you go, this brush is here for algorithmic demonstration purposes. "
"Don't lose sleep because you can't figure out what it's for, it Really "
"doesn't do much. For the sake of description, here's what it does:"
msgstr ""
"在旧版软件中，我们包含这个引擎的目的主要还是展示替代算法，它并没有专门为了某"
"种艺术用途进行过优化。为了满足你的好奇心，你可以在下图中看到它的工作效果："

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:29
msgid ".. image:: images/brushes/Krita-tutorial7-C.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:30
msgid ""
"Yeah, that's it, a round brush with some chalky texture, and the option to "
"fade in opacity and saturation. That's it."
msgstr ""
"正如你所见的那样，一个普通的圆形笔刷，带有一点粉笔纹理，然后可以对不透明度和"
"饱和度选项进行淡入淡出。仅此而已。"
