msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_contributors_manual."
"pot\n"

#: ../../contributors_manual.rst:5
msgid "Contributors Manual"
msgstr "参与者手册"

#: ../../contributors_manual.rst:7
msgid "Everything you need to know to help out with Krita!"
msgstr ""
"参与 Krita 项目工作所需的全部知识。(注意：Technical Pages 页面无法翻译)"

#: ../../contributors_manual.rst:9
msgid "Contents:"
msgstr "目录："
