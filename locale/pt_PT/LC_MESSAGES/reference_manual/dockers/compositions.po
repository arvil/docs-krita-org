# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 16:49+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: docker en icons image Kritamouseright images alt\n"
"X-POFile-SpellExtra: Composition mouseright dockers\n"

#: ../../<generated>:1
msgid "Rename composition"
msgstr "Mudar o nome da composição"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/dockers/compositions.rst:1
msgid "Overview of the compositions docker."
msgstr "Introdução à área de composições."

#: ../../reference_manual/dockers/compositions.rst:12
#: ../../reference_manual/dockers/compositions.rst:17
msgid "Compositions"
msgstr "Composições"

#: ../../reference_manual/dockers/compositions.rst:19
msgid ""
"The compositions docker allows you to save the configurations of your layers "
"being visible and invisible, allowing you to save several configurations of "
"your layers."
msgstr ""
"A área de composições permite-lhe gravar as configurações das suas camadas "
"que estão visíveis ou invisíveis, permitindo-lhe gravar várias configurações "
"das suas camadas."

#: ../../reference_manual/dockers/compositions.rst:22
msgid ".. image:: images/dockers/Composition-docker.png"
msgstr ".. image:: images/dockers/Composition-docker.png"

#: ../../reference_manual/dockers/compositions.rst:24
msgid "Adding new compositions"
msgstr "Adicionar novas composições"

#: ../../reference_manual/dockers/compositions.rst:24
msgid ""
"You do this by setting your layers as you wish, then pressing the plus sign. "
"If you had a word in the text-box to the left, this will be the name of your "
"new composition."
msgstr ""
"Poderá fazer isto se configurar as suas camadas como desejar, carregando "
"depois no sinal 'mais'. Se tiver uma palavra no campo de texto à esquerda, "
"este será o nome da sua nova composição."

#: ../../reference_manual/dockers/compositions.rst:26
msgid "Activating composition"
msgstr "Activar a composição"

#: ../../reference_manual/dockers/compositions.rst:27
msgid "Double-click the composition name to switch to that composition."
msgstr "Faça duplo-click sobre o nome da composição para mudar para a mesma."

#: ../../reference_manual/dockers/compositions.rst:28
msgid "Removing compositions"
msgstr "Remover composições"

#: ../../reference_manual/dockers/compositions.rst:29
msgid "The minus sign. Select a composition, and hit this button to remove it."
msgstr ""
"O sinal de 'menos'. Seleccione uma composição e carregue neste botão para a "
"remover."

#: ../../reference_manual/dockers/compositions.rst:30
msgid "Exporting compositions"
msgstr "Composições"

#: ../../reference_manual/dockers/compositions.rst:31
msgid "The file sign. Will export all checked compositions."
msgstr "O símbolo do ficheiro. Irá exportar todas as composições assinaladas."

#: ../../reference_manual/dockers/compositions.rst:32
msgid "Updating compositions"
msgstr "Actualizar as composições"

#: ../../reference_manual/dockers/compositions.rst:33
msgid ""
"|mouseright| a composition to overwrite it with the current configuration."
msgstr ""
"Use o |mouseright| sobre uma composição para a substituir pela configuração "
"actual."

#: ../../reference_manual/dockers/compositions.rst:35
msgid "|mouseright| a composition to rename it."
msgstr "Use o |mouseright| sobre uma composição para mudar o nome dela."
