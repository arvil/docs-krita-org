# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-25 12:13+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: XOR BlendmodesNOTCONVERSEmap image\n"
"X-POFile-SpellExtra: BlendingmodesNANDGradients images binary\n"
"X-POFile-SpellExtra: BlendingmodesNOTIMPLICATIONGradients\n"
"X-POFile-SpellExtra: BlendmodesNOTIMPLICATIONmap blendingmodes\n"
"X-POFile-SpellExtra: BlendingmodesORGradients BlendmodesIMPLIESmap\n"
"X-POFile-SpellExtra: BlendmodesXNORmap BlendingmodesNOTCONVERSEGradients\n"
"X-POFile-SpellExtra: XNOR BlendingmodesIMPLIESGradients\n"
"X-POFile-SpellExtra: BlendingmodesCONVERSEGradients BlendmodesNANDmap\n"
"X-POFile-SpellExtra: BlendingmodesXNORGradients BlendmodesXORmap\n"
"X-POFile-SpellExtra: BlendmodesNORmap BlendmodesCONVERSEmap\n"
"X-POFile-SpellExtra: BlendmodesORmap Krita BlendmodesANDmap\n"
"X-POFile-SpellExtra: BlendingmodesNORGradients BlendingmodesXORGradients\n"
"X-POFile-SpellExtra: BlendingmodesANDGradients\n"

#: ../../reference_manual/blending_modes/binary.rst:1
msgid "Page about the binary blending modes in Krita:"
msgstr "Página sobre os modos de mistura binários no Krita:"

#: ../../reference_manual/blending_modes/binary.rst:10
#: ../../reference_manual/blending_modes/binary.rst:14
msgid "Binary"
msgstr "Binário"

#: ../../reference_manual/blending_modes/binary.rst:16
msgid ""
"Binary modes are special class of blending modes which utilizes binary "
"operators for calculations. Binary modes are unlike every other blending "
"modes as these modes have a fractal attribute with falloff similar to other "
"blending modes. Binary modes can be used for generation of abstract art "
"using layers with very smooth surface. All binary modes have capitalized "
"letters to distinguish themselves from other blending modes."
msgstr ""
"Os modos binários são uma classe especial de modos de mistura que usam "
"operadores binários nos cálculos. Os modos binários são completamente "
"diferentes dos outros modos de mistura na medida em que têm um atributo "
"fractal com um corte ou decaimento semelhante aos outros modos de mistura. "
"Os modos binários podem ser usados para a geração de arte abstracta com "
"camadas numa superfície muito suave. Todos os modos binários têm letras "
"maiúsculas para os distinguir dos outros modos de mistura."

#: ../../reference_manual/blending_modes/binary.rst:18
msgid ""
"To clarify on how binary modes works, convert decimal values to binary "
"values, then treat 1 or 0 as T or F respectively, and use binary operation "
"to get the end result, and then convert the result back to decimal."
msgstr ""
"Para clarificar como funcionam os modos binários, converta os valores "
"decimais para binários, tratando depois o 1 ou 0 como V ou F, "
"respectivamente, usando uma operação binária para obter o resultado final, "
"convertendo depois o resultado de volta para decimal."

#: ../../reference_manual/blending_modes/binary.rst:22
msgid ""
"Binary blending modes do not work on float images or negative numbers! So, "
"don't report bugs about using binary modes on unsupported color space."
msgstr ""
"Os modos de mistura binários não funcionam em imagens de vírgula flutuante "
"ou números negativos! Como tal, não relate erros sobre o uso de modos "
"binários num espaço de cores não suportado."

#: ../../reference_manual/blending_modes/binary.rst:24
#: ../../reference_manual/blending_modes/binary.rst:28
msgid "AND"
msgstr "E"

#: ../../reference_manual/blending_modes/binary.rst:30
msgid ""
"Performs the AND operation for the base and blend layer. Similar to multiply "
"blending mode."
msgstr ""
"Efectua a operação E para a camada de base e de mistura. Semelhante ao modo "
"de mistura por multiplicação."

#: ../../reference_manual/blending_modes/binary.rst:35
msgid ".. image:: images/blending_modes/binary/Blend_modes_AND_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_AND_map.png"

#: ../../reference_manual/blending_modes/binary.rst:35
#: ../../reference_manual/blending_modes/binary.rst:40
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **AND**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **E**."

#: ../../reference_manual/blending_modes/binary.rst:40
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_AND_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_AND_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:41
#: ../../reference_manual/blending_modes/binary.rst:46
msgid "CONVERSE"
msgstr "CONVERSÃO"

#: ../../reference_manual/blending_modes/binary.rst:48
msgid ""
"Performs the inverse of IMPLICATION operation for the base and blend layer. "
"Similar to screen mode with blend layer and base layer inverted."
msgstr ""
"Efectua o inverso da operação IMPLICAÇÃO para a camada de base e a de "
"mistura. É semelhante ao modo de ecrã com a camada de mistura e a camada de "
"base invertida."

#: ../../reference_manual/blending_modes/binary.rst:53
msgid ".. image:: images/blending_modes/binary/Blend_modes_CONVERSE_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_CONVERSE_map.png"

#: ../../reference_manual/blending_modes/binary.rst:53
#: ../../reference_manual/blending_modes/binary.rst:58
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **CONVERSE**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: "
"**CONVERSÃO**."

#: ../../reference_manual/blending_modes/binary.rst:58
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_CONVERSE_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_CONVERSE_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:60
#: ../../reference_manual/blending_modes/binary.rst:64
msgid "IMPLICATION"
msgstr "IMPLICAÇÃO"

#: ../../reference_manual/blending_modes/binary.rst:66
msgid ""
"Performs the IMPLICATION operation for the base and blend layer. Similar to "
"screen mode with base layer inverted."
msgstr ""
"Efectua a operação IMPLICAÇÃO para a camada de base e de mistura. É "
"semelhante ao modo de ecrã com a camada de base invertida."

#: ../../reference_manual/blending_modes/binary.rst:71
msgid ".. image:: images/blending_modes/binary/Blend_modes_IMPLIES_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_IMPLIES_map.png"

#: ../../reference_manual/blending_modes/binary.rst:71
#: ../../reference_manual/blending_modes/binary.rst:76
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **IMPLICATION**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: "
"**IMPLICAÇÃO**."

#: ../../reference_manual/blending_modes/binary.rst:76
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_IMPLIES_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_IMPLIES_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:78
#: ../../reference_manual/blending_modes/binary.rst:82
msgid "NAND"
msgstr "NÃO-E"

#: ../../reference_manual/blending_modes/binary.rst:84
msgid ""
"Performs the inverse of AND operation for base and blend layer. Similar to "
"the inverted multiply mode."
msgstr ""
"Efectua o inverso da operação E para a camada de base e de mistura. É "
"semelhante ao modo de multiplicação invertido."

#: ../../reference_manual/blending_modes/binary.rst:89
msgid ".. image:: images/blending_modes/binary/Blend_modes_NAND_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_NAND_map.png"

#: ../../reference_manual/blending_modes/binary.rst:89
#: ../../reference_manual/blending_modes/binary.rst:94
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NAND**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **NÃO-"
"E**."

#: ../../reference_manual/blending_modes/binary.rst:94
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_NAND_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NAND_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:96
#: ../../reference_manual/blending_modes/binary.rst:100
msgid "NOR"
msgstr "NÃO-OU"

#: ../../reference_manual/blending_modes/binary.rst:102
msgid ""
"Performs the inverse of OR operation for base and blend layer. Similar to "
"the inverted screen mode."
msgstr ""
"Efectua o inverso da operação OU para a camada de base e de mistura. É "
"semelhante ao modo de ecrã invertido."

#: ../../reference_manual/blending_modes/binary.rst:107
msgid ".. image:: images/blending_modes/binary/Blend_modes_NOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_NOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:107
#: ../../reference_manual/blending_modes/binary.rst:112
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOR**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **NÃO-"
"OU**."

#: ../../reference_manual/blending_modes/binary.rst:112
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_NOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NOR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:114
#: ../../reference_manual/blending_modes/binary.rst:118
msgid "NOT CONVERSE"
msgstr "NÃO-CONVERSÃO"

#: ../../reference_manual/blending_modes/binary.rst:120
msgid ""
"Performs the inverse of CONVERSE operation for base and blend layer. Similar "
"to the multiply mode with base layer and blend layer inverted."
msgstr ""
"Efectua o inverso da operação CONVERSÃO para a camada de base e de mistura. "
"É semelhante ao modo de multiplicação com a camada de base e de mistura "
"invertidas."

#: ../../reference_manual/blending_modes/binary.rst:125
msgid ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_CONVERSE_map.png"
msgstr ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_CONVERSE_map.png"

#: ../../reference_manual/blending_modes/binary.rst:125
#: ../../reference_manual/blending_modes/binary.rst:130
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOT CONVERSE**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **NÃO-"
"CONVERSÃO**."

#: ../../reference_manual/blending_modes/binary.rst:130
msgid ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_CONVERSE_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_CONVERSE_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:132
#: ../../reference_manual/blending_modes/binary.rst:136
msgid "NOT IMPLICATION"
msgstr "NÃO-IMPLICAÇÃO"

#: ../../reference_manual/blending_modes/binary.rst:138
msgid ""
"Performs the inverse of IMPLICATION operation for base and blend layer. "
"Similar to the multiply mode with the blend layer inverted."
msgstr ""
"Efectua o inverso da operação IMPLICAÇÃO para a camada de base e de mistura. "
"É semelhante ao modo de multiplicação com a camada de mistura invertida."

#: ../../reference_manual/blending_modes/binary.rst:143
msgid ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_IMPLICATION_map.png"
msgstr ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_IMPLICATION_map.png"

#: ../../reference_manual/blending_modes/binary.rst:143
#: ../../reference_manual/blending_modes/binary.rst:148
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOT IMPLICATION**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **NÃO-"
"IMPLICAÇÃO**."

#: ../../reference_manual/blending_modes/binary.rst:148
msgid ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_IMPLICATION_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_IMPLICATION_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:150
#: ../../reference_manual/blending_modes/binary.rst:154
msgid "OR"
msgstr "OU"

#: ../../reference_manual/blending_modes/binary.rst:156
msgid ""
"Performs the OR operation for base and blend layer. Similar to screen mode."
msgstr ""
"Efectua a operação OU para a camada de base e de mistura. É semelhante ao "
"modo de ecrã."

#: ../../reference_manual/blending_modes/binary.rst:161
msgid ".. image:: images/blending_modes/binary/Blend_modes_OR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_OR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:161
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **OR**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **OU**."

#: ../../reference_manual/blending_modes/binary.rst:166
msgid ".. image:: images/blending_modes/binary/Blending_modes_OR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_OR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:166
#: ../../reference_manual/blending_modes/binary.rst:179
#: ../../reference_manual/blending_modes/binary.rst:184
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **XOR**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **OU-"
"EXCLUSIVO**."

#: ../../reference_manual/blending_modes/binary.rst:168
#: ../../reference_manual/blending_modes/binary.rst:172
msgid "XOR"
msgstr "XOR"

#: ../../reference_manual/blending_modes/binary.rst:174
msgid ""
"Performs the XOR operation for base and blend layer. This mode has a special "
"property that if you duplicate the blend layer twice, you get the base layer."
msgstr ""
"Efectua a operação XOR (OU-EXCLUSIVO) para a camada de base e de mistura. "
"Este modo tem a propriedade especial de, caso duplique a camada de mistura, "
"irá obter a camada de base."

#: ../../reference_manual/blending_modes/binary.rst:179
msgid ".. image:: images/blending_modes/binary/Blend_modes_XOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_XOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:184
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_XOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_XOR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:186
#: ../../reference_manual/blending_modes/binary.rst:190
msgid "XNOR"
msgstr "NÃO-OU Exclusivo"

#: ../../reference_manual/blending_modes/binary.rst:192
msgid ""
"Performs the XNOR operation for base and blend layer. This mode has a "
"special property that if you duplicate the blend layer twice, you get the "
"base layer."
msgstr ""
"Efectua a operação XNOR (NÃO-OU-EXCLUSIVO) para a camada de base e de "
"mistura. Este modo tem a propriedade especial de, caso duplique a camada de "
"mistura, irá obter a camada de base."

#: ../../reference_manual/blending_modes/binary.rst:197
msgid ".. image:: images/blending_modes/binary/Blend_modes_XNOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_XNOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:197
#: ../../reference_manual/blending_modes/binary.rst:202
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **XNOR**."
msgstr ""
"Esquerda: **Camada de Base**. Centro: **Camada de Mistura**. Direita: **NÃO-"
"OU-EXCLUSIVO**."

#: ../../reference_manual/blending_modes/binary.rst:202
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_XNOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_XNOR_Gradients.png"
