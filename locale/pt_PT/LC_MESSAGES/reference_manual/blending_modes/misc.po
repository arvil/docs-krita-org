# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:22+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: BlendingmodesCopyGreenSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesCopySampleimagewithdots Krita\n"
"X-POFile-SpellExtra: BlendingmodesDissolveSampleimagewithdots image\n"
"X-POFile-SpellExtra: BlendingmodesCopyBlueSampleimagewithdots\n"
"X-POFile-SpellExtra: blendingmodes images\n"
"X-POFile-SpellExtra: BlendingmodesCopyRedSampleimagewithdots\n"
"X-POFile-SpellExtra: KritaFilterlayerinvertgreenchannel misc\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""
"Página sobre os diversos modos de mistura no Krita: Mapa de Relevo, Mapa da "
"Normal Combinado, Copiar o Vermelho, Copiar o Verde, Copiar o Azul, Copiar e "
"Dissolver."

#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr "Diversos"

#: ../../reference_manual/blending_modes/misc.rst:17
msgid "Bumpmap (Blending Mode)"
msgstr "Mapa de Relevo (Modo de Mistura)"

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr "Mapa de Relevo"

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr ""
"Este filtro parece multiplicar e respeitar o valor do 'alfa' à entrada."

#: ../../reference_manual/blending_modes/misc.rst:25
#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr "Combinar o Mapa da Normal"

#: ../../reference_manual/blending_modes/misc.rst:25
msgid "Normal Map"
msgstr "Mapa da Normal"

#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <https://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""
"Um modo de mistura robusto a nível matemático para os mapas da normal, "
"usando a `Mistura do Mapa da Normal Reorientado <https://blog.selfshadow.com/"
"publications/blending-in-detail/>`_."

#: ../../reference_manual/blending_modes/misc.rst:34
msgid "Copy (Blending Mode)"
msgstr "Cópia (Modo de Mistura)"

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr "Copiar"

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr ""
"Copia a camada anterior exactamente. É útil quando usar filtros e máscaras "
"de filtragem."

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr "Esquerda: **Normal**. Direita: **Cópia**."

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Red"
msgstr "Copiar o Vermelho"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Green"
msgstr "Copiar o Verde"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Blue"
msgstr "Copiar o Azul"

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr "Copiar o Vermelho, Verde, Azul"

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr ""
"Isto é um modo de mistura que irá apenas copiar/misturar um canal de origem "
"com um canal de destino. Em específico, irá pegar no canal específico da "
"camada superior e copiá-lo para as camadas inferiores."

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""
"Por isso, se quiser que o pincel só afecte o canal vermelho, configure o "
"modo de mistura para 'copiar o vermelho'."

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"
msgstr ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Os modos de mistura por cópia do vermelho, verde e azul também funcionam nas "
"camadas de filtragem."

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""
"Isto também pode ser feito com as camadas de filtragem. Por isso, se quiser "
"inverter rapidamente o canal verde de uma camada, crie uma camada de "
"filtragem de inversão com o 'copiar o verde' por cima."

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr "Esquerda: **Normal**. Direita: **Copiar o Vermelho**."

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr "Esquerda: **Normal**. Direita: **Copiar o Verde**."

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr "Esquerda: **Normal**. Direita: **Copiar o Azul**."

#: ../../reference_manual/blending_modes/misc.rst:86
#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr "Dissolver"

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr ""
"Em vez de usar a transparência, este modo de mistura irá usar um padrão de "
"meios-tons aleatório para que as áreas transparentes tenham de facto uma "
"espécie de transparência."

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr "Esquerda: **Normal**. Direita: **Dissolver**."
