# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 13:56+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/mirror_tools.rst:1
msgid "How to use the canvas mirroring tools in Krita."
msgstr "Hur man använder dukens speglingsverktyg i Krita."

#: ../../user_manual/mirror_tools.rst:11
msgid "Mirror"
msgstr "Spegla"

#: ../../user_manual/mirror_tools.rst:11
msgid "Symmetry"
msgstr "Symmetri"

#: ../../user_manual/mirror_tools.rst:16
msgid "Mirror Tools"
msgstr "Speglingsverktyg"

#: ../../user_manual/mirror_tools.rst:18
msgid ""
"Draw on one side of a mirror line while the Mirror Tool copies the results "
"to the other side. The Mirror Tools are accessed along the toolbar. You can "
"move the location of the mirror line by grabbing the handle."
msgstr ""
"Rita på en sida av en speglingslinje medan speglingsverktyget kopierar "
"resultatet till andra sidan. Speglingsverktygen är tillgängliga längs "
"verktygsraden. Man kan flytta speglingslinjens plats genom att ta tag i "
"greppet."

#: ../../user_manual/mirror_tools.rst:21
msgid ".. image:: images/Mirror-tool.png"
msgstr ".. image:: images/Mirror-tool.png"

#: ../../user_manual/mirror_tools.rst:22
msgid ""
"Mirror Tools give a similar result to the :ref:`multibrush_tool`, but unlike "
"the Multibrush which only traces brush strokes like the :ref:"
"`freehand_brush_tool`, the Mirror Tools can be used with any other tool that "
"traces strokes, such as the :ref:`line_tool` and the :ref:`path_tool`, and "
"even with the Multibrush Tool."
msgstr ""
"Speglingsverktyg ger liknande resultat som :ref:`multibrush_tool`, men i "
"motsats till multipenseln, som bara följer penseldrag såsom  :ref:"
"`freehand_brush_tool`, kan speglingsverktygen användas med vilket annat "
"verktyg som helst som drar streck, såsom :ref:`line_tool` och :ref:"
"`path_tool`, och även med multipenselverktyget."

#: ../../user_manual/mirror_tools.rst:24
msgid ""
"**Horizontal Mirror Tool** - Mirror the results along the horizontal axis."
msgstr ""
"**Horisontellt speglingsverktyg** - Spegla resultatet längs den horisontella "
"axeln."

#: ../../user_manual/mirror_tools.rst:27
msgid "**Vertical Mirror Tool** - Mirror the results along the vertical axis."
msgstr ""
"**Vertikalt speglingsverktyg** - Spegla resultatet längs den vertikala axeln."

#: ../../user_manual/mirror_tools.rst:30
msgid ""
"There are additional options for each tool. You can access these by the "
"clicking the drop-down arrow located on the right of each tool."
msgstr ""
"Det finns ytterligare alternativ för varje verktyg. Man kan komma åt dem "
"genom att klicka på kombinationspilen placerad till höger om varje verktyg."

#: ../../user_manual/mirror_tools.rst:33
msgid ""
"Hide Mirror Line (toggle) -- Locks the mirror axis and hides the axis line."
msgstr ""
"Dölj speglingslinje (växlar) - Låser speglingsaxeln och döljer axellinjen."

#: ../../user_manual/mirror_tools.rst:35
msgid "Lock (toggle) - hides the move icon on the axis line."
msgstr "Lås (växlar) - Döljer förflyttningsikonen på axellinjen."

#: ../../user_manual/mirror_tools.rst:36
msgid ""
"Move to Canvas Center - Moves the axis line to the center of the canvas."
msgstr "Flytta till dukens mitt - Flyttar axellinjen till dukens mitt."

#: ../../user_manual/mirror_tools.rst:40
msgid "Mirroring along a rotated line"
msgstr "Spegling kring en roterad linje"

#: ../../user_manual/mirror_tools.rst:42
msgid ""
"The Mirror Tool can only mirror along a perfectly vertical or horizontal "
"line. To mirror along a line that is at a rotated angle, use the :ref:"
"`multibrush_tool` and its various parameters, it has more advanced options "
"besides basic symmetry."
msgstr ""
"Speglingsverktyget kan bara spegla kring en helt vertikal eller horisontell "
"linje. För att spegla kring en linje som har en roterad vinkel, använd :ref:"
"`multibrush_tool` och dess olika parametrar. Den har mer avancerade "
"alternativ förutom grundläggande symmetri."
