# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:30+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: frihandspenselverktyg"

#: ../../user_manual/getting_started/starting_krita.rst:None
msgid ".. image:: images/Starting-krita.png"
msgstr ".. image:: images/Starting-krita.png"

#: ../../user_manual/getting_started/starting_krita.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"En enkel handledning till de första grundläggande stegen i att använda "
"Krita: skapa och spara en bild."

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Getting started"
msgstr "Komma igång"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Save"
msgstr "Spara"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Load"
msgstr "Läs in"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "New"
msgstr "Ny"

#: ../../user_manual/getting_started/starting_krita.rst:22
msgid "Starting Krita"
msgstr "Starta Krita"

#: ../../user_manual/getting_started/starting_krita.rst:24
msgid ""
"When you start Krita for the first time there will be no canvas or new "
"document open by default. You will be greeted by a :ref:`welcome screen "
"<welcome_screen>`, which will have option to create a new file or open "
"existing document. To create a new canvas you have to create a new document "
"from the :guilabel:`File` menu or by clicking on :guilabel:`New File`  under "
"start section of the welcome screen. This will open the new file dialog box. "
"If you want to open an existing image, either use :menuselection:`File --> "
"Open` or drag the image from your computer into Krita's window."
msgstr ""
"När man startar Krita första gången finns det normalt ingen duk eller nytt "
"dokument öppnat. Man hälsas av en :ref:`välkomstskärm <welcome_screen>`, som "
"har alternativen att skapa en ny fil eller öppna ett befintligt dokument. "
"För att skapa en ny duk måste ett nytt dokument skapas i menyn  :guilabel:"
"`Arkiv` eller genom att klicka på :guilabel:`Ny fil` i startsektionen på "
"välkomstskärmen. Det visar dialogrutan för ny fil. Om man vill öppna en "
"befintlig bild, använd antingen :menuselection:`Arkiv --> Öppna` eller dra "
"bilden från datorn till Kritas fönster."

#: ../../user_manual/getting_started/starting_krita.rst:37
msgid "Creating a New Document"
msgstr "Skapa ett nytt dokument"

#: ../../user_manual/getting_started/starting_krita.rst:39
msgid "A new document can be created as follows."
msgstr "Ett nytt dokument kan skapas på följande sätt."

#: ../../user_manual/getting_started/starting_krita.rst:41
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr "Klicka på :guilabel:`Arkiv` i programmenyn längst upp."

#: ../../user_manual/getting_started/starting_krita.rst:42
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing the :kbd:`Ctrl "
"+ N` shortcut."
msgstr ""
"Klicka därefter på :guilabel:`Ny`. Eller så kan du göra det genom att trycka "
"på genvägen :kbd:`Ctrl + N`."

#: ../../user_manual/getting_started/starting_krita.rst:43
msgid "Now you will get a New Document dialog box as shown below:"
msgstr "Nu ser du dialogrutan Nytt dokument som visas nedan:"

#: ../../user_manual/getting_started/starting_krita.rst:46
msgid ".. image:: images/Krita_newfile.png"
msgstr ".. image:: images/Krita_newfile.png"

#: ../../user_manual/getting_started/starting_krita.rst:47
msgid ""
"Click on the :guilabel:`Custom Document` section and in the :guilabel:"
"`Dimensions` tab choose A4 (300ppi) or any size that you prefer from the :"
"guilabel:`predefined` drop down To know more about the other sections such "
"as create document from clipboard and templates see :ref:"
"`create_new_document`."
msgstr ""
"Klicka på sektionen :guilabel:`Eget dokument` och välj A4 (300 ppi) under "
"fliken  :guilabel:`Dimensioner` eller den storlek som du föredrar i "
"kombinationsrutan :guilabel:`Fördefinierad`"

#: ../../user_manual/getting_started/starting_krita.rst:52
msgid ""
"Make sure that the color profile is RGB and depth is set to 8-bit integer/"
"channel in the color section. For advanced information about the color and "
"color management refer to :ref:`general_concept_color`."
msgstr ""
"Säkerställ att färgprofilen är RGB och att djup är inställt till 8-bitars "
"heltal per kanal i färgsektionen. För avancerad information om färghantering "
"se :ref:`general_concept_color`."

#: ../../user_manual/getting_started/starting_krita.rst:56
msgid "How to use brushes"
msgstr "Hur man använder penslar"

#: ../../user_manual/getting_started/starting_krita.rst:58
msgid ""
"Now, on the blank white canvas, just left click with your mouse or draw with "
"the pen on a graphic tablet. If everything's correct, you should be able to "
"draw on the canvas! The brush tool should be selected by default when you "
"start Krita, but if for some reason it is not, you can click on this |"
"toolfreehandbrush| icon from the toolbox and activate the brush tool."
msgstr ""
"Vänsterklicka nu bara med musen eller rita med pennan på en ritplatta på den "
"tomma vita duken. Om allting är riktigt, ska man kunna rita på duken. "
"Penselverktyget ska normalt vara valt när Krita startas, men om det av någon "
"anledning inte är det, kan man klicka på ikonen |toolfreehandbrush| i "
"verktygslådan och aktivera penselverktyget."

#: ../../user_manual/getting_started/starting_krita.rst:64
msgid ""
"Of course, you'd want to use different brushes. On your right, there's a "
"docker named Brush Presets (or on top, press the :kbd:`F6` key to find this "
"one) with all these cute squares with pens and crayons."
msgstr ""
"Man vill förstås använda olika penslar. Till höger finns en panel som heter "
"Förinställningar av penslar (eller längst upp, tryck på tangenten :kbd:`F6` "
"för att hitta den) med alla snygga fyrkanter med pennor och kritor."

#: ../../user_manual/getting_started/starting_krita.rst:68
msgid ""
"If you want to tweak the presets, check the Brush Editor in the toolbar. You "
"can also access the Brush Editor with the :kbd:`F5` key."
msgstr ""
"Om man vill justera förinställningarna, titta på penseleditorn i "
"verktygsraden. Man kan också komma åt penseleditorn med tangenten :kbd:`F5`."

#: ../../user_manual/getting_started/starting_krita.rst:72
msgid ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"
msgstr ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"

#: ../../user_manual/getting_started/starting_krita.rst:73
msgid ""
"Tick any of the squares to choose a brush, and then draw on the canvas. To "
"change color, click the triangle in the Advanced Color Selector docker."
msgstr ""
"Markera någon av fyrkanterna för att välja en pensel, och rita sedan på "
"duken. Klicka på triangeln i den avancerade färgväljaren för att ändra färg."

#: ../../user_manual/getting_started/starting_krita.rst:77
msgid "Erasing"
msgstr "Radering"

#: ../../user_manual/getting_started/starting_krita.rst:79
msgid ""
"There are brush presets for erasing, but it is often faster to use the "
"eraser toggle. By toggling the :kbd:`E` key, your current brush switches "
"between erasing and painting. This erasing method works with most of the "
"tools. You can erase using the line tool, rectangle tool, and even the "
"gradient tool."
msgstr ""
"Det finns penselförinställningar för radering, men det går oftast fortare "
"att använda raderingsläget. Genom att trycka på tangenten :kbd:`E` byter den "
"aktuella penseln mellan att radera och måla. Den här raderingsmetoden "
"fungerar med de flesta verktygen. Man kan radera med linjeverktyget, "
"rektangelverktyget, och till och med toningsverktyget."

#: ../../user_manual/getting_started/starting_krita.rst:85
msgid "Saving and opening files"
msgstr "Spara och öppna filer"

#: ../../user_manual/getting_started/starting_krita.rst:87
msgid ""
"Now, once you have figured out how to draw something in Krita, you may want "
"to save it. The save option is in the same place as it is in all other "
"computer programs: the top-menu of :guilabel:`File`, and then :guilabel:"
"`Save`. Select the folder you want to have your drawing, and select the file "
"format you want to use ('.kra' is Krita's default format, and will save "
"everything). And then hit :guilabel:`Save`. Some older versions of Krita "
"have a bug and require you to manually type the extension."
msgstr ""
"När man nu har räknat ut hur man ritar någonting i Krita, kanske man vill "
"spara det. Alternativet för att spara finns på samma ställe som i de flesta "
"andra datorprogram: :guilabel:`Arkiv` på den övre menyraden och sedan  :"
"guilabel:`Spara`. Välj katalogen där teckningen ska finnas, och välj "
"filformat som ska användas ('.kra' är Kritas standardformat, och det sparar "
"allting). Klicka sedan på :guilabel:`Spara`. Vissa äldre versioner av Krita "
"har ett fel som kräver att man skriver in filändelsen manuellt."

#: ../../user_manual/getting_started/starting_krita.rst:95
msgid ""
"If you want to show off your image on the internet, check out the :ref:"
"`saving_for_the_web` tutorial."
msgstr ""
"Om du vill visa upp din bild på Internet, ta då en titt på handledningen :"
"ref:`saving_for_the_web`."

#: ../../user_manual/getting_started/starting_krita.rst:98
msgid ""
"Check out :ref:`navigation` for further basic information, :ref:"
"`basic_concepts` for an introduction as Krita as a medium, or just go out "
"and explore Krita!"
msgstr ""
"Ta en titt på :ref:`navigation` för ytterligare grundläggande information,  :"
"ref:`basic_concepts` för en introduktion till Krita som medium, eller sätt "
"bara igång att utforska Krita!"

#~ msgid "Custom Document"
#~ msgstr "Eget dokument"

#~ msgid "Create From Clipboard"
#~ msgstr "Skapa från klippbord"

#~ msgid "Templates:"
#~ msgstr "Mallar:"
