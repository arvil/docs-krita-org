# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:30+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/getting_started/installation.rst:1
msgid "Detailed steps on how to install Krita"
msgstr "Detaljerade steg för att installera Krita"

#: ../../user_manual/getting_started/installation.rst:14
#: ../../user_manual/getting_started/installation.rst:18
msgid "Installation"
msgstr "Installation"

#: ../../user_manual/getting_started/installation.rst:21
msgid "Windows"
msgstr "Windows"

#: ../../user_manual/getting_started/installation.rst:22
msgid ""
"Windows users can download Krita from the website, the Windows Store, or "
"Steam."
msgstr ""
"Användare av Windows kan ladda ner Krita från webbplatsen, Windows Store "
"eller Steam."

#: ../../user_manual/getting_started/installation.rst:24
msgid ""
"The versions on the Store and Steam cost money, but are `functionally "
"identical <https://krita.org/en/item/krita-available-from-the-windows-store/"
">`_ to the (free) website version. Unlike the website version, however, both "
"paid versions get automatic updates when new versions of Krita comes out. "
"After deduction of the Store fee, the purchase cost supports Krita "
"development."
msgstr ""
"Versionerna på Store och Steam kostar pengar, men är `funktionellt identiska "
"<https://krita.org/en/item/krita-available-from-the-windows-store/>`_ med "
"den (fria) webbplatsversionen. I motsats till webbplatsversionen ger dock "
"båda betalade versionerna automatiska uppdateringar när nya versioner av "
"Krita kommer ut. Efter avdrag av avgiften från Store stöder inköpskostnaden "
"utveckling av Krita."

#: ../../user_manual/getting_started/installation.rst:31
msgid ""
"The latest version is always on our `website <https://krita.org/download/>`_."
msgstr ""
"Den senaste versionen finns alltid på vår `webbplats <https://krita.org/"
"download/>`_."

#: ../../user_manual/getting_started/installation.rst:33
msgid ""
"The page will try to automatically recommend the correct architecture (64- "
"or 32-bit), but you can select \"All Download Versions\" to get more "
"choices. To determine your computer architecture manually, go to :"
"menuselection:`Settings --> About`. Your architecture will be listed as the :"
"guilabel:`System Type` in the :guilabel:`Device Specifications` section."
msgstr ""
"Sidan försöker automatiskt rekommendera korrekt arkitektur (64- eller 32-"
"bitar), men man kan välja \"All Download Versions\" för att få fler val. För "
"att manuellt bestämma datorarkitektur, gå till  :menuselection:"
"`Inställningar --> Om`. Arkitekturen listas som :guilabel:`Systemtyp` i "
"sektionen :guilabel:`Enhetsspecifikationer`."

#: ../../user_manual/getting_started/installation.rst:35
msgid ""
"Krita by default downloads an **installer EXE**, but you can also download a "
"**portable zip-file** version instead. Unlike the installer version, this "
"portable version does not show previews in Windows Explorer automatically. "
"To get these previews with the portable version, also install Krita's "
"**Windows Shell Extension** extension (available on the download page)."
msgstr ""
"Krita laddar normalt ner en **installations-EXE**, men man kan också ladda "
"ner en **portabel zip-fil** istället. I motsats till installationsversionen, "
"visar inte den portabla versionen automatiskt förhandsgranskningar i Windows "
"utforskare. För att också få förhandsgranskningarna med den portabla "
"versionen, installera också Kritas utökning **Windows Shell Extension** "
"(tillgänglig på nerladdningssidan)."

#: ../../user_manual/getting_started/installation.rst:36
msgid "Website:"
msgstr "Webbplats:"

#: ../../user_manual/getting_started/installation.rst:37
msgid ""
"These files are also available from the `KDE download directory <https://"
"download.kde.org/stable/krita/>`_."
msgstr ""
"Filerna är också tillgängliga från `KDE:s nerladdningskatalog <https://"
"download.kde.org/stable/krita/>`_."

#: ../../user_manual/getting_started/installation.rst:38
msgid "Windows Store:"
msgstr "Windows Store:"

#: ../../user_manual/getting_started/installation.rst:39
msgid ""
"For a small fee, you can download Krita `from the Windows Store <https://www."
"microsoft.com/store/productId/9N6X57ZGRW96>`_. This version requires Windows "
"10."
msgstr ""
"För en liten avgift, kan man ladda ner Krita `från Windows Store <https://"
"www.microsoft.com/sv-se/p/krita/9n6x57zgrw96>`_. Versionen kräver Windows 10."

#: ../../user_manual/getting_started/installation.rst:41
msgid ""
"For a small fee, you can also download Krita `from Steam <https://store."
"steampowered.com/app/280680/Krita/>`_."
msgstr ""
"För en liten avgift, kan man också ladda ner Krita `från Steam <https://"
"store.steampowered.com/app/280680/Krita/>`_."

#: ../../user_manual/getting_started/installation.rst:42
msgid "Steam:"
msgstr "Steam:"

#: ../../user_manual/getting_started/installation.rst:44
msgid ""
"To download a portable version of Krita go to the `KDE <https://download.kde."
"org/stable/krita/>`_ download directory and get the zip-file instead of the "
"setup.exe installer."
msgstr ""
"För att ladda ner en portabel version av Krita, gå till "
"nerladdningskatalogen för `KDE <https://download.kde.org/stable/krita/>`_ "
"och hämta zip-filen istället för installationsprogrammet setup.exe."

#: ../../user_manual/getting_started/installation.rst:48
msgid ""
"Krita requires Windows 7 or newer. The Store version requires Windows 10."
msgstr ""
"Krita kräver Windows 7 eller senare. Versionen på Store kräver Windows 10."

#: ../../user_manual/getting_started/installation.rst:51
msgid "Linux"
msgstr "Linux"

#: ../../user_manual/getting_started/installation.rst:53
msgid ""
"Many Linux distributions package the latest version of Krita. Sometimes you "
"will have to enable an extra repository. Krita runs fine under most desktop "
"enviroments such as KDE, Gnome, LXDE, Xfce etc. -- even though it is a KDE "
"application and needs the KDE libraries. You might also want to install the "
"KDE system settings module and tweak the gui theme and fonts used, depending "
"on your distributions"
msgstr ""
"Många Linux-distributioner paketerar den senaste versionen av Krita. Ibland "
"måste man aktivera ett extra arkiv. Krita kör bra med de flesta "
"skrivbordsmiljöer såsom KDE, Gnome, LXDE, Xfce, etc., även om det är ett KDE-"
"program och behöver KDE-bibliotek. Man kanske också vill installera KDE:s "
"systeminställningsmodul och justera det grafiska användargränssnittets tema "
"och teckensnitt som används, beroende på distribution."

#: ../../user_manual/getting_started/installation.rst:61
msgid "Nautilus/Nemo file extensions"
msgstr "Nautilus/Nemo filutökningar"

#: ../../user_manual/getting_started/installation.rst:63
msgid ""
"Since April 2016, KDE's Dolphin file manager shows kra and ora thumbnails by "
"default, but Nautilus and it's derivatives need an extension. `We recommend "
"Moritz Molch's extensions for XCF, KRA, ORA and PSD thumbnails <https://"
"moritzmolch.com/1749>`__."
msgstr ""
"Sedan april 2016, visar KDE:s Dolphin filhanterare normalt kra och ora "
"miniatyrbilder, men Nautilus och dess derivativ kräver en utökning.  `Vi "
"rekommenderar Moritz Molchs utökning för XCF, KRA, ORA och PSD "
"miniatyrbilder <https://moritzmolch.com/1749>`_."

#: ../../user_manual/getting_started/installation.rst:69
msgid "Appimages"
msgstr "Appimages"

#: ../../user_manual/getting_started/installation.rst:71
msgid ""
"For Krita 3.0 and later, first try out the appimage from the website. **90% "
"of the time this is by far the easiest way to get the latest Krita.** Just "
"download the appimage, and then use the file properties or the bash command "
"chmod to make the appimage executable. Double click it, and enjoy Krita. (Or "
"run it in the terminal with ./appimagename.appimage)"
msgstr ""
"Prova först en appimage från webbplatsen för Krita 3.0 och senare. **90 % av "
"tiden är det överlägset det enklaste sättet att hämta senaste Krita.** Ladda "
"bara ner en appimage, och använd sedan filegenskaperna eller bash-kommandot "
"chmod för att göra appimage körbar. Dubbelklicka på den, och njut av Krita "
"(eller kör den i en terminal med ./appimage-namn.appimage)."

#: ../../user_manual/getting_started/installation.rst:78
msgid "Open the terminal into the folder you have the appimage."
msgstr "Öppna terminalen i katalogen där denna appimage finns."

#: ../../user_manual/getting_started/installation.rst:79
msgid "Make it executable:"
msgstr "Gör den körbar:"

#: ../../user_manual/getting_started/installation.rst:83
msgid "chmod a+x krita-3.0-x86_64.appimage"
msgstr "chmod a+x krita-3.0-x86_64.appimage"

#: ../../user_manual/getting_started/installation.rst:85
msgid "Run Krita!"
msgstr "Kör Krita!"

#: ../../user_manual/getting_started/installation.rst:89
msgid "./krita-3.0-x86_64.appimage"
msgstr "./krita-3.0-x86_64.appimage"

#: ../../user_manual/getting_started/installation.rst:91
msgid ""
"Appimages are ISOs with all the necessary libraries bundled inside, that "
"means no fiddling with repositories and dependencies, at the cost of a "
"slight bit more diskspace taken up (And this size would only be bigger if "
"you were using Plasma to begin with)."
msgstr ""
"En appimage är en ISO-fil med alla nödvändiga bibliotek packade inne i dem, "
"vilket betyder inget pillande med arkiv och beroenden, på bekostnad av något "
"mer diskutrymme (och storleken är ännu större om man använder Plasma till "
"att börja med)."

#: ../../user_manual/getting_started/installation.rst:97
msgid "Ubuntu and Kubuntu"
msgstr "Ubuntu och Kubuntu"

#: ../../user_manual/getting_started/installation.rst:99
msgid ""
"It does not matter which version of Ubuntu you use, Krita will run just "
"fine. However, by default, only a very old version of Krita is available. "
"You should either use the appimage, flatpak or the snap available from "
"Ubuntu's app store. We also maintain a ppa for getting latest builds of "
"Krita, you can read more about the ppa and install instructions `here "
"<https://launchpad.net/~kritalime/+archive/ubuntu/ppa>`_."
msgstr ""
"Det spelar ingen roll vilken version av Ubuntu man använder, Krita kör bra "
"ändå. Dock är normalt bara en mycket gammal version av Krita tillgänglig. "
"Man bör antingen använda appimage, flatpak eller snap tillgängliga från "
"Ubuntus app store. Vi underhåller också en ppa för att hämta Kritas senaste "
"byggen. Man kan läsa mer om ppa och installationsinstruktioner `här <https://"
"launchpad.net/~kritalime/+archive/ubuntu/ppa>`_."

#: ../../user_manual/getting_started/installation.rst:106
msgid "OpenSUSE"
msgstr "OpenSUSE"

#: ../../user_manual/getting_started/installation.rst:108
msgid "The latest stable builds are available from KDE:Extra repo:"
msgstr "Senaste stabila byggen är tillgängliga från arkivet KDE:Extra:"

#: ../../user_manual/getting_started/installation.rst:110
msgid "https://download.opensuse.org/repositories/KDE:/Extra/"
msgstr "https://download.opensuse.org/repositories/KDE:/Extra/"

#: ../../user_manual/getting_started/installation.rst:113
msgid "Krita is also in the official repos, you can install it from Yast."
msgstr ""
"Krita finns också i de officiella arkiven. Man kan installera det från Yast."

#: ../../user_manual/getting_started/installation.rst:116
msgid "Fedora"
msgstr "Fedora"

#: ../../user_manual/getting_started/installation.rst:118
msgid ""
"Krita is in the official repos, you can install it by using packagekit (Add/"
"Remove Software) or by writing the following command in terminal."
msgstr ""
"Krita finns i de officiella arkiven. Man kan installera det genom att "
"använda packagekit (Lägg till/ta bort programvara) eller genom att skriva "
"följande kommandon i en terminal."

#: ../../user_manual/getting_started/installation.rst:120
msgid "``dnf install krita``"
msgstr "``dnf install krita``"

#: ../../user_manual/getting_started/installation.rst:122
msgid ""
"You can also use the software center such as gnome software center or "
"Discover to install Krita."
msgstr ""
"Man kan också använda ett programvarucenter som Gnome programvarucenter "
"eller Discover för att installera Krita."

#: ../../user_manual/getting_started/installation.rst:125
msgid "Debian"
msgstr "Debian"

#: ../../user_manual/getting_started/installation.rst:127
msgid ""
"The latest version of Krita available in Debian is 3.1.1. To install Krita "
"type the following line in terminal:"
msgstr ""
"Den senaste versionen av Krita tillgänglig i Debian är 3.1.1. För att "
"installera Krita skriv in följande rad i en terminal:"

#: ../../user_manual/getting_started/installation.rst:130
msgid "``apt install krita``"
msgstr "``apt install krita``"

#: ../../user_manual/getting_started/installation.rst:134
msgid "Arch"
msgstr "Arch"

#: ../../user_manual/getting_started/installation.rst:136
msgid ""
"Arch Linux provides krita package in the Extra repository. You can install "
"Krita by using the following command:"
msgstr ""
"Arch Linux tillhandahåller krita-paket i arkivet Extra. Man kan installera "
"Krita genom att använda följande kommando:"

#: ../../user_manual/getting_started/installation.rst:139
msgid "``pacman -S krita``"
msgstr "``pacman -S krita``"

#: ../../user_manual/getting_started/installation.rst:141
msgid ""
"You can also find Krita pkgbuild in arch user repositories but it is not "
"guaranteed to contain the latest git version."
msgstr ""
"Man hittar också Krita pkgbuild i arch användararkiv men det garanteras inte "
"att det innehåller den senaste git-versionen."

#: ../../user_manual/getting_started/installation.rst:145
msgid "OS X"
msgstr "OS X"

#: ../../user_manual/getting_started/installation.rst:147
msgid ""
"You can download the latest binary from our `website <https://krita.org/"
"download/krita-desktop/>`__. The binaries work only with Mac OSX version "
"10.12 and newer."
msgstr ""
"Man kan ladda ner den senaste binärfilen från vår  `webbsida <https://krita."
"org/download/krita-desktop/>`__. Binärfilerna fungerar bara med Mac OSX-"
"version 10.12 och senare."

#: ../../user_manual/getting_started/installation.rst:152
msgid "Source"
msgstr "Källkod"

#: ../../user_manual/getting_started/installation.rst:154
msgid ""
"While it is certainly more difficult to compile Krita from source than it is "
"to install from prebuilt packages, there are certain advantages that might "
"make the effort worth it:"
msgstr ""
"Även om det visserligen är svårare att kompilera Krita från källkod än att "
"installera från förbyggda paket, finns det vissa fördelar som kan göra det "
"värt besväret:"

#: ../../user_manual/getting_started/installation.rst:158
msgid ""
"You can follow the development of Krita on the foot. If you compile Krita "
"regularly from the development repository, you will be able to play with all "
"the new features that the developers are working on."
msgstr ""
"Du kan följa Kritas utveckling när den pågår. Om du kompilerar Krita "
"regelbundet från utvecklingsversionen kan du leka med alla de senaste "
"funktionerna som utvecklarna håller på med."

#: ../../user_manual/getting_started/installation.rst:161
msgid ""
"You can compile it optimized for your processor. Most pre-built packages are "
"built for the lowest-common denominator."
msgstr ""
"Du kan kompilera det optimerat för din processor. De flesta förbyggda paket "
"är skapade för den minsta gemensamma nämnaren."

#: ../../user_manual/getting_started/installation.rst:163
msgid "You will be getting all the bug fixes as soon as possible as well."
msgstr "Du får också alla felrättningar så snart som möjligt."

#: ../../user_manual/getting_started/installation.rst:164
msgid ""
"You can help the developers by giving us your feedback on features as they "
"are being developed and you can test bug fixes for us. This is hugely "
"important, which is why our regular testers get their name in the about box "
"just like developers."
msgstr ""
"Du kan hjälpa utvecklarna genom att ge oss återmatning om funktioner medan "
"de utvecklas och du kan testa felrättningar åt oss. Det är ytterst viktigt, "
"vilket är orsaken till att våra regelbundna testare också har sina namn i om-"
"rutan, precis som utvecklarna."

#: ../../user_manual/getting_started/installation.rst:169
msgid ""
"Of course, there are also some disadvantages: when building from the current "
"development source repository you also get all the unfinished features. It "
"might mean less stability for a while, or things shown in the user interface "
"that don't work. But in practice, there is seldom really bad instability, "
"and if it is, it's easy for you to go back to a revision that does work."
msgstr ""
"Det finns förstås några nackdelar: när man bygger från det nuvarande "
"utvecklingsarkivet får man också alla ofärdiga funktionerna. Det kan betyda "
"att stabiliteten är sämre en viss tid, eller att saker dyker upp i "
"användargränssnittet som inte fungerar. Men i praktiken är det sällan "
"riktigt allvarlig instabilitet, och om så är fallet är det enkelt att gå "
"tillbaka till en version som fungerar."

#: ../../user_manual/getting_started/installation.rst:176
msgid ""
"So... If you want to start compiling from source, begin with the latest "
"build instructions from the guide :ref:`here <building_krita>`."
msgstr ""
"Alltså ... Om du vill börja kompilera från källkod, börja med de senaste "
"bygginstruktionerna från handledningen :ref:`här <building_krita>`."

#: ../../user_manual/getting_started/installation.rst:179
msgid ""
"If you encounter any problems, or if you are new to compiling software, "
"don't hesitate to contact the Krita developers. There are three main "
"communication channels:"
msgstr ""
"Om du stöter på några problem, eller om du är nybörjare på att kompilera "
"programvara, tveka inte att kontakta Kritas utvecklare. Det finns tre "
"huvudsakliga kommunikationskanaler:"

#: ../../user_manual/getting_started/installation.rst:183
msgid "irc: irc.freenode.net, channel #krita"
msgstr "irc: irc.freenode.net, channel #krita"

#: ../../user_manual/getting_started/installation.rst:184
msgid "`mailing list <https://mail.kde.org/mailman/listinfo/kimageshop>`__"
msgstr "`e-postlista <https://mail.kde.org/mailman/listinfo/kimageshop>`__"

#: ../../user_manual/getting_started/installation.rst:185
msgid "`forums <https://forum.kde.org/viewforum.php?f=136>`__"
msgstr "`forum <https://forum.kde.org/viewforum.php?f=136>`__"

#~ msgid ""
#~ "Krita requires Windows Vista or newer. INTEL GRAPHICS CARD USERS: IF YOU "
#~ "SEE A BLACK OR BLANK WINDOW: UPDATE YOUR DRIVERS!"
#~ msgstr ""
#~ "Krita kräver Windows Vista eller senare. ANVÄNDARE AV INTEL GRAFIKKORT: "
#~ "OM NI SER ETT SVART ELLER TOMT FÖNSTER: UPPDATERA ERA DRIVRUTINER!"
