# Translation of docs_krita_org_reference_manual___blending_modes___lighten.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-09-01 16:52+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../reference_manual/blending_modes/lighten.rst:1
msgid ""
"Page about the lighten blending modes in Krita: Color Dodge, Gamma "
"Illumination, Gamma Light, Easy Dodge, Flat Light, Fog Lighten, Hard Light, "
"Lighten, Lighter Color, Linear Dodge, Linear Light, P-Norm A, P-Norm B, Pin "
"Light, Screen, Soft Light, Tint and Vivid Light."
msgstr ""
"Pàgina sobre els modes de barreja més lleugers al Krita: Esvaïment del "
"color, Il·luminació gamma, Llum gamma, Esvaïment fàcil, Llum plana, Boira "
"clara, Llum forta, Aclarit, Color més clar, Esvaïment lineal, Llum lineal, P-"
"Norm A, P-Norm B, Llum focal, Pantalla, Llum suau, Tint i Llum vívida."

#: ../../reference_manual/blending_modes/lighten.rst:16
#: ../../reference_manual/blending_modes/lighten.rst:83
msgid "Lighten"
msgstr "Aclarit"

#: ../../reference_manual/blending_modes/lighten.rst:18
msgid "Blending modes that lighten the image."
msgstr "Modes de barreja que aclareixen la imatge."

#: ../../reference_manual/blending_modes/lighten.rst:20
#: ../../reference_manual/blending_modes/lighten.rst:24
msgid "Color Dodge"
msgstr "Esvaïment del color"

#: ../../reference_manual/blending_modes/lighten.rst:20
msgid "Dodge"
msgstr "Esvaeix"

#: ../../reference_manual/blending_modes/lighten.rst:26
msgid ""
"Similar to Divide. Inverts the top layer, and divides the lower layer by the "
"inverted top layer. This results in a image with emphasized highlights, like "
"Dodging would do in traditional darkroom photography."
msgstr ""
"Similar a Divideix. Inverteix la capa superior i divideix la capa inferior "
"per la capa superior invertida. Això dóna com a resultat una imatge amb "
"èmfasi en les llums intenses, com ho faria Esvaeix en la cambra fosca de la "
"fotografia tradicional."

#: ../../reference_manual/blending_modes/lighten.rst:33
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Color_Dodge_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Color_Dodge_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:33
msgid "Left: **Normal**. Right: **Color Dodge**."
msgstr "Esquerra: **Normal**. Dreta: **Esvaïment del color**."

#: ../../reference_manual/blending_modes/lighten.rst:35
#: ../../reference_manual/blending_modes/lighten.rst:39
msgid "Gamma Illumination"
msgstr "Il·luminació gamma"

#: ../../reference_manual/blending_modes/lighten.rst:41
msgid "Inverted Gamma Dark blending mode."
msgstr "Mode de barreja Gamma fosca invertit."

#: ../../reference_manual/blending_modes/lighten.rst:46
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Gamma_Illumination_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Gamma_Illumination_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:46
msgid "Left: **Normal**. Right: **Gamma Illumination**."
msgstr "Esquerra: **Normal**. Dreta: **Il·luminació gamma**."

#: ../../reference_manual/blending_modes/lighten.rst:49
#: ../../reference_manual/blending_modes/lighten.rst:53
msgid "Gamma Light"
msgstr "Llum gamma"

#: ../../reference_manual/blending_modes/lighten.rst:55
msgid "Outputs the upper layer as power of the lower layer."
msgstr "Mostra la capa superior com a potència de la capa inferior."

#: ../../reference_manual/blending_modes/lighten.rst:60
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Gamma_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Gamma_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:60
msgid "Left: **Normal**. Right: **Gamma Light**."
msgstr "Esquerra: **Normal**. Dreta: **Llum gamma**."

#: ../../reference_manual/blending_modes/lighten.rst:62
#: ../../reference_manual/blending_modes/lighten.rst:66
msgid "Hard Light"
msgstr "Llum forta"

#: ../../reference_manual/blending_modes/lighten.rst:68
msgid ""
"Similar to Overlay. A combination of the Multiply and Screen blending modes, "
"switching between both at a middle-lightness."
msgstr ""
"Similar a la superposició. Una combinació dels modes de barreja Multiplica i "
"Pantalla, canviant entre tots dos en una claredat mitjana."

#: ../../reference_manual/blending_modes/lighten.rst:71
msgid ""
"Hard light checks if the color on the upperlayer has a lightness above 0.5. "
"Unlike overlay, if the pixel is lighter than 0.5, it is blended like in "
"Multiply mode, if not the pixel is blended like in Screen mode."
msgstr ""
"La llum forta comprova si el color a la capa superior té una claredat "
"superior al 0,5. A diferència de la superposició, si el píxel és més clar "
"que 0,5, es barrejarà com en la mode Multiplica, de ser així es barrejarà "
"com en el mode Pantalla."

#: ../../reference_manual/blending_modes/lighten.rst:73
msgid "Effectively, this decreases contrast."
msgstr "Efectivament, això fa disminuir el contrast."

#: ../../reference_manual/blending_modes/lighten.rst:78
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Hard_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Hard_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:78
msgid "Left: **Normal**. Right: **Hard Light**."
msgstr "Esquerra: **Normal**. Dreta: **Llum forta**."

#: ../../reference_manual/blending_modes/lighten.rst:85
msgid ""
"With the darken, the upper layer's colors are checked for their lightness. "
"Only if they are Lighter than the underlying color on the lower layer, will "
"they be visible."
msgstr ""
"Amb l'enfosquiment, els colors de la capa superior es verifiquen per la seva "
"claredat. Només seran visibles si són més clars que el color subjacent a la "
"capa inferior."

#: ../../reference_manual/blending_modes/lighten.rst:90
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Lighten_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Lighten_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:90
msgid "Left: **Normal**. Right: **Lighten**."
msgstr "Esquerra: **Normal**. Dreta: **Aclarit**."

#: ../../reference_manual/blending_modes/lighten.rst:95
msgid "Lighter Color"
msgstr "Color més clar"

#: ../../reference_manual/blending_modes/lighten.rst:100
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Lighter_Color_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Lighter_Color_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:100
msgid "Left: **Normal**. Right: **Lighter Color**."
msgstr "Esquerra: **Normal**. Dreta: **Color més clar**."

#: ../../reference_manual/blending_modes/lighten.rst:106
msgid "Linear Dodge"
msgstr "Esvaïment lineal"

#: ../../reference_manual/blending_modes/lighten.rst:108
msgid "Exactly the same as :ref:`bm_addition`."
msgstr "Exactament el mateix que :ref:`bm_addition`."

#: ../../reference_manual/blending_modes/lighten.rst:110
msgid "Put in for compatibility purposes."
msgstr "Posat per propòsits de compatibilitat."

#: ../../reference_manual/blending_modes/lighten.rst:115
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Dodge_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Dodge_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:115
msgid ""
"Left: **Normal**. Right: **Linear Dodge** (exactly the same as Addition)."
msgstr ""
"Esquerra: **Normal**. Dreta: **Esvaïment lineal** (exactament el mateix que "
"l'addició)."

#: ../../reference_manual/blending_modes/lighten.rst:120
msgid "Easy Dodge"
msgstr "Esvaïment fàcil"

#: ../../reference_manual/blending_modes/lighten.rst:122
msgid ""
"Aims to solve issues with Color Dodge blending mode by using a formula which "
"falloff is similar to Dodge, but the falloff rate is softer. It is within "
"the range of 0.0f and 1.0f unlike Color Dodge mode."
msgstr ""
"L'objectiu és resoldre els problemes amb el mode de barreja Esvaïment del "
"color, s'utilitza una fórmula en la qual el decaïment és similar a "
"Esvaïment, però la velocitat del decaïment és més suau. Es troba dintre de "
"l'interval de 0,0f i 1,0f a diferència del mode Esvaïment del color."

#: ../../reference_manual/blending_modes/lighten.rst:127
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Easy_Dodge_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Easy_Dodge_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:127
msgid "Left: **Normal**. Right: **Easy Dodge**."
msgstr "Esquerra: **Normal**. Dreta: **Esvaïment fàcil**."

#: ../../reference_manual/blending_modes/lighten.rst:132
msgid "Flat Light"
msgstr "Llum plana"

#: ../../reference_manual/blending_modes/lighten.rst:134
msgid ""
"The spreadout variation of Vivid Light mode which range is between 0.0f and "
"1.0f."
msgstr ""
"La variació disseminada del mode Llum vívida, el seu interval es troba entre "
"0,0f i 1,0f."

#: ../../reference_manual/blending_modes/lighten.rst:139
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Flat_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Flat_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:139
msgid "Left: **Normal**. Right: **Flat Light**."
msgstr "Esquerra: **Normal**. Dreta: **Llum plana**."

#: ../../reference_manual/blending_modes/lighten.rst:144
msgid "Fog Lighten (IFS Illusions)"
msgstr "Boira clara (Il·lusions IFS)"

#: ../../reference_manual/blending_modes/lighten.rst:146
msgid ""
"Lightens the image in a way that there is a 'fog' in the end result. This is "
"due to the unique property of fog lighten in which midtones combined are "
"lighter than non-midtones blend."
msgstr ""
"Aclareix la imatge de manera que hi hagi una «boira» al resultat final. Això "
"es deu a la propietat única d'aclariment que té la boira, en la qual els "
"tons mitjans combinats són més clars que la barreja de tons que no són "
"mitjans."

#: ../../reference_manual/blending_modes/lighten.rst:151
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Fog_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Fog_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:151
msgid "Left: **Normal**. Right: **Fog Lighten**."
msgstr "Esquerra: **Normal**. Dreta: **Boira clara**."

#: ../../reference_manual/blending_modes/lighten.rst:156
msgid "Linear Light"
msgstr "Llum lineal"

#: ../../reference_manual/blending_modes/lighten.rst:158
msgid "Similar to :ref:`bm_overlay`."
msgstr "Similar al :ref:`bm_overlay`."

#: ../../reference_manual/blending_modes/lighten.rst:160
msgid ""
"Combines :ref:`bm_linear_dodge` and :ref:`bm_linear_burn`. When the "
"lightness of the upper-pixel is higher than 0.5, it uses Linear dodge, if "
"not, Linear burn to blend the pixels."
msgstr ""
"Combina :ref:`bm_linear_dodge` i :ref:`bm_linear_burn`. Quan la claredat del "
"píxel superior és superior que 0,5, s'utilitzarà l'Esvaïment lineal, de no "
"ser així s'emprarà el cremat lineal per a barrejar els píxels."

#: ../../reference_manual/blending_modes/lighten.rst:165
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Light_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Light_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/lighten.rst:165
#: ../../reference_manual/blending_modes/lighten.rst:170
#: ../../reference_manual/blending_modes/lighten.rst:175
msgid "Left: **Normal**. Right: **Linear Light**."
msgstr "Esquerra: **Normal**. Dreta: **Llum lineal**."

#: ../../reference_manual/blending_modes/lighten.rst:170
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Light_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Light_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/lighten.rst:175
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Linear_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:180
msgid "P-Norm A"
msgstr "P-Norm A"

#: ../../reference_manual/blending_modes/lighten.rst:182
msgid ""
"P-Norm A is similar to Screen blending mode which slightly darken images, "
"and the falloff is more consistent all-around in terms of outline of values. "
"Can be used an alternative to screen blending mode at times."
msgstr ""
"El P-Norm A és similar al mode de barreja Pantalla, el qual enfosqueix "
"lleugerament les imatges, i el decaïment és més consistent en termes de "
"contorns. De vegades pot usar-se com a alternativa al mode de barreja "
"pantalla."

#: ../../reference_manual/blending_modes/lighten.rst:187
msgid ""
".. image:: images/blending_modes/lighten/Blending_modes_P-"
"Norm_A_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/Blending_modes_P-"
"Norm_A_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:187
msgid "Left: **Normal**. Right: **P-Norm A**."
msgstr "Esquerra: **Normal**. Dreta: **P-Norm A**."

#: ../../reference_manual/blending_modes/lighten.rst:192
msgid "P-Norm B"
msgstr "P-Norm B"

#: ../../reference_manual/blending_modes/lighten.rst:194
msgid ""
"P-Norm B is similar to Screen blending mode which slightly darken images, "
"and the falloff is more consistent all-around in terms of outline of values. "
"The falloff is sharper in P-Norm B than in P-Norm A. Can be used an "
"alternative to screen blending mode at times."
msgstr ""
"El P-Norm B és similar al mode de barreja Pantalla, el qual enfosqueix "
"lleugerament les imatges, i el decaïment és més consistent en termes de "
"contorns. El decaïment és més agut al P-Norm B que al P-Norm A. De vegades "
"pot usar-se com a alternativa al mode de barreja pantalla."

#: ../../reference_manual/blending_modes/lighten.rst:199
msgid ""
".. image:: images/blending_modes/lighten/Blending_modes_P-"
"Norm_B_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/Blending_modes_P-"
"Norm_B_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:199
msgid "Left: **Normal**. Right: **P-Norm B**."
msgstr "Esquerra: **Normal**. Dreta: **P-Norm B**."

#: ../../reference_manual/blending_modes/lighten.rst:204
msgid "Pin Light"
msgstr "Llum focal"

#: ../../reference_manual/blending_modes/lighten.rst:206
msgid ""
"Checks which is darker the lower layer's pixel or the upper layer's double "
"so bright. Then checks which is brighter of that result or the inversion of "
"the doubled lower layer."
msgstr ""
"Comprova quins són els valors que proporcionen un píxel més fosc: el valor "
"del píxel a la capa inferior o el de la capa superior amb doble brillantor. "
"Després verifica quina és més brillant entre aquest resultat i la inversió "
"de la capa inferior amb doble brillantor."

#: ../../reference_manual/blending_modes/lighten.rst:212
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Pin_Light_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Pin_Light_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/lighten.rst:212
#: ../../reference_manual/blending_modes/lighten.rst:217
#: ../../reference_manual/blending_modes/lighten.rst:222
msgid "Left: **Normal**. Right: **Pin Light**."
msgstr "Esquerra: **Normal**. Dreta: **Llum focal**."

#: ../../reference_manual/blending_modes/lighten.rst:217
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Pin_Light_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Pin_Light_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/lighten.rst:222
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Pin_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Pin_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:227
msgid "Screen"
msgstr "Pantalla"

#: ../../reference_manual/blending_modes/lighten.rst:229
msgid "Perceptually the opposite of :ref:`bm_multiply`."
msgstr "Perceptiblement el contrari que :ref:`bm_multiply`."

#: ../../reference_manual/blending_modes/lighten.rst:231
msgid ""
"Mathematically, Screen takes both layers, inverts them, then multiplies "
"them, and finally inverts them again."
msgstr ""
"Matemàticament, Pantalla pren ambdues capes, les inverteix, després les "
"multiplica i finalment les inverteix de nou."

#: ../../reference_manual/blending_modes/lighten.rst:233
msgid ""
"This results in light tones being more opaque and dark tones transparent."
msgstr ""
"Això fa que els tons clars siguin més opacs i els tons foscos transparents."

#: ../../reference_manual/blending_modes/lighten.rst:238
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Screen_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Screen_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/lighten.rst:238
#: ../../reference_manual/blending_modes/lighten.rst:243
#: ../../reference_manual/blending_modes/lighten.rst:248
msgid "Left: **Normal**. Right: **Screen**."
msgstr "Esquerra: **Normal**. Dreta: **Pantalla**."

#: ../../reference_manual/blending_modes/lighten.rst:243
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Screen_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Screen_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/lighten.rst:248
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Screen_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Screen_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:253
msgid "Soft Light (Photoshop) & Soft Light SVG"
msgstr "Llum suau (Photoshop) i Llum suau SVG"

#: ../../reference_manual/blending_modes/lighten.rst:255
msgid ""
"These are less harsh versions of Hard Light, not resulting in full black or "
"full white."
msgstr ""
"Aquestes són versions menys dures que la Llum forta, que no donen com a "
"resultat el negre o blanc total."

#: ../../reference_manual/blending_modes/lighten.rst:257
msgid ""
"The SVG version is slightly different to the Photoshop version in that it "
"uses a slightly different bit of formula when the lightness of the lower "
"pixel is lower than 25%, this prevents the strength of the brightness "
"increase."
msgstr ""
"La versió SVG és lleugerament diferent de la versió del Photoshop, ja que "
"utilitza una fórmula lleugerament diferent quan la claredat del píxel "
"inferior és inferior al 25%, això evita que la intensitat de la brillantor "
"augmenti."

#: ../../reference_manual/blending_modes/lighten.rst:262
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_Photoshop_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_Photoshop_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:262
msgid "Left: **Normal**. Right: **Soft Light (Photoshop)**."
msgstr "Esquerra: **Normal**. Dreta: **Llum suau (Photoshop)**."

#: ../../reference_manual/blending_modes/lighten.rst:268
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_SVG_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_SVG_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:268
msgid "Left: **Normal**. Right: **Soft Light (SVG)**."
msgstr "Esquerra: **Normal**. Dreta: **Llum suau (SVG)**."

#: ../../reference_manual/blending_modes/lighten.rst:271
msgid "Soft Light (IFS Illusions) & Soft Light (Pegtop-Delphi)"
msgstr "Llum suau (il·lusions IFS) i Llum suau (Pegtop-Delphi)"

#: ../../reference_manual/blending_modes/lighten.rst:273
msgid ""
"These are alternative versions of standard softlight modes which are made to "
"solve discontinuities seen with the standard blend modes. Sometimes, these "
"modes offer subtle advantages by offering more contrast within some areas, "
"and these advantages are more or less noticeable within different color "
"spaces and depth."
msgstr ""
"Aquestes són versions alternatives dels modes de llum suau estàndard que es "
"fan per a resoldre les discontinuïtats observades amb els modes de barreja "
"estàndard. De vegades, aquests modes ofereixen avantatges subtils en oferir "
"més contrast en algunes àrees, i aquests avantatges són més o menys notables "
"en diferents espais de color i profunditats."

#: ../../reference_manual/blending_modes/lighten.rst:278
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_IFS_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_IFS_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:278
msgid "Left: **Normal**. Right: **Soft Light (IFS Illusions)**."
msgstr "Esquerra: **Normal**. Dreta: **Llum suau (il·lusions IFS)**."

#: ../../reference_manual/blending_modes/lighten.rst:284
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_PEGTOP_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Soft_Light_PEGTOP_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:284
msgid "Left: **Normal**. Right: **Soft Light (Pegtop-Delphi)**."
msgstr "Esquerra: **Normal**. Dreta: **Llum suau (Pegtop-Delphi)**."

# skip-rule: k-Super
#: ../../reference_manual/blending_modes/lighten.rst:289
msgid "Super Light"
msgstr "Superllum"

#: ../../reference_manual/blending_modes/lighten.rst:291
msgid ""
"Smoother variation of Hard Light blending mode with more contrast in it."
msgstr "Variació més suau del mode de barreja Llum forta amb més contrast."

#: ../../reference_manual/blending_modes/lighten.rst:296
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Super_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Super_Light_Sample_image_with_dots.png"

# skip-rule: k-Super
#: ../../reference_manual/blending_modes/lighten.rst:296
msgid "Left: **Normal**. Right: **Super Light**."
msgstr "Esquerra: **Normal**. Dreta: **Superllum**."

#: ../../reference_manual/blending_modes/lighten.rst:301
msgid "Tint (IFS Illusions)"
msgstr "Tint (il·lusions IFS)"

#: ../../reference_manual/blending_modes/lighten.rst:303
msgid ""
"Basically, the blending mode only ends in shades of tints. This means that "
"it's very useful for painting light colors while still in the range of tints."
msgstr ""
"Bàsicament, el mode de barreja només finalitza a les ombres dels tints. Això "
"vol dir que és molt útil per a pintar colors d'ombrejat mentre es troba dins "
"d'un cert interval de tintes."

#: ../../reference_manual/blending_modes/lighten.rst:308
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Tint_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Tint_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:308
msgid "Left: **Normal**. Right: **Tint**."
msgstr "Esquerra: **Normal**. Dreta: **Tint**."

#: ../../reference_manual/blending_modes/lighten.rst:313
msgid "Vivid Light"
msgstr "Llum vívida"

#: ../../reference_manual/blending_modes/lighten.rst:315
msgid "Similar to Overlay."
msgstr "Similar a la superposició."

#: ../../reference_manual/blending_modes/lighten.rst:317
msgid ""
"Mixes both Color Dodge and Burn blending modes. If the color of the upper "
"layer is darker than 50%, the blending mode will be Burn, if not the "
"blending mode will be Color Dodge."
msgstr ""
"Mescla ambdós modes de barreja Esvaïment del color i Cremat del color. Si el "
"color de la capa superior és més fosc que el 50%, el mode de barreja serà "
"Cremat del color, d'altra manera el mode de barreja serà Esvaïment del color."

#: ../../reference_manual/blending_modes/lighten.rst:321
msgid ""
"This algorithm doesn't use color dodge and burn, we don't know WHAT it does "
"do but for Color Dodge and Burn you need to use :ref:`bm_hard_mix`."
msgstr ""
"Aquest algorisme no utilitza Esvaïment del color i Cremat del color, no "
"sabem QUÈ fa, però per a Esvaïment del color i Cremat del color necessitareu "
"utilitzar la :ref:`bm_hard_mix`."

#: ../../reference_manual/blending_modes/lighten.rst:326
msgid ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Vivid_Light_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/lighten/"
"Blending_modes_Vivid_Light_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/lighten.rst:326
msgid "Left: **Normal**. Right: **Vivid Light**."
msgstr "Esquerra: **Normal**. Dreta: **Llum vívida**."
